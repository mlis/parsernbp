/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.report;

/**
 *
 * @author marcin
 */
public enum RateComparableStates {

    COMPARABLE_BY_DATE,
    COMPARABLE_BY_PURCHASE_RATE,
    COMPARABLE_BY_SELLING_RATE;
}
