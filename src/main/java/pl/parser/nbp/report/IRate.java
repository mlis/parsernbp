
package pl.parser.nbp.report;

import java.util.Date;

/**
 *
 * @author marcin
 */
public interface IRate extends Comparable{
    public Double getPurchaseRate();
    public Double getSellingRate();
    public IRate setComparableState(RateComparableStates state);
    public RateComparableStates getComparableState();
    public Date getDate();
}
