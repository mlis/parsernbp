package pl.parser.nbp.report;

import java.text.DecimalFormat;
import java.util.List;

/**
 *
 * @author marcin
 */
public interface ICurrencyRateReport {

    public List<IRate> getRates();
    
    public DecimalFormat getSourceDecimalFormat();
    
    public IRate getFirstRate();

    public IRate getLastRate();

    public IRate getHighestSellingRate();

    public IRate getLowestSellingRate();

    public IRate getLowestPurchaseRate();

    public IRate getHighestPurchaseRate();

    public double getSellingRateChange();

    public double getPurchaseRateChange();

    public double getAvaragePurchaseRate();

    public double getAvarageSellingRate();

    public double getPurchaseRateStandardDeviation();

    public double getSellingRateStandardDeviation();

    public String getCurrencyCode();
}
