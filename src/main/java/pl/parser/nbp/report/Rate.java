package pl.parser.nbp.report;

import java.util.Date;

/**
 *
 * @author marcin
 */
public class Rate implements IRate {

    private final Double purchaseRate;
    private final Double sellingRate;
    private final Date date;
    private RateComparableStates state = RateComparableStates.COMPARABLE_BY_DATE;

    public Rate(Date date, double purchaseRate, double sellingRate) {
        this.purchaseRate = new Double(purchaseRate);
        this.sellingRate = new Double(sellingRate);
        this.date = date;
    }

    @Override
    public Double getPurchaseRate() {
        return purchaseRate;
    }

    @Override
    public Double getSellingRate() {
        return sellingRate;
    }

    @Override
    public Date getDate() {
        return date;
    }

    @Override
    public IRate setComparableState(RateComparableStates state) {
        this.state = state;
        return this;
    }

    @Override
    public RateComparableStates getComparableState() {
        return state;
    }

    @Override
    public int compareTo(Object o) {
        IRate toCompare = (IRate) o;
        int result;
        switch (state) {
            case COMPARABLE_BY_PURCHASE_RATE:
                result = this.getPurchaseRate().compareTo(toCompare.getPurchaseRate());
                break;
            case COMPARABLE_BY_SELLING_RATE:
                result = this.getSellingRate().compareTo(toCompare.getSellingRate());
                break;
            default:
                result = this.getDate().compareTo(toCompare.getDate());
                break;
        }
        return result;
    }
}
