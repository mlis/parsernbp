package pl.parser.nbp.report;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;
import org.apache.commons.math.stat.descriptive.moment.Mean;
import org.apache.commons.math.stat.descriptive.moment.StandardDeviation;

/**
 *
 * @author marcin
 */
public class CurrencyRateReport implements ICurrencyRateReport {

    private String currencyCode;
    private List<IRate> rates;
    private Double avaragePurchaseRate = null;
    private Double avarageSelingRate = null;
    private Double purchaseRateStdDev = null;
    private Double sellingRateStdDev = null;
    private Double sellingRateChange = null;
    private Double purchaseRateChange = null;
    private IRate firstRate = null;
    private IRate lastRate = null;
    private IRate highestPurchaseRate = null;
    private IRate highestSellingRate = null;
    private IRate lowestPurchaseRate = null;
    private IRate lowestSellingRate = null;
    private StandardDeviation stdDev = new StandardDeviation();
    private Mean mean = new Mean();
    private DecimalFormat srcDecFrm;

    /**
     * 
     * @param currencyCode 
     * @param rates
     * @param sourceDecimalFormat Format in which source data was stored - indicate reliable accuracy for data presentation
     */
    public CurrencyRateReport(String currencyCode, List<IRate> rates, DecimalFormat sourceDecimalFormat) {
        this.currencyCode = currencyCode;
        this.rates = rates;
        this.srcDecFrm = sourceDecimalFormat;
    }

    @Override
    public List<IRate> getRates() {
        return Collections.unmodifiableList(rates);
    }

    @Override
    public String getCurrencyCode() {
        return currencyCode;
    }

    @Override
    public double getAvaragePurchaseRate() {
        if (avaragePurchaseRate == null) {
            avaragePurchaseRate = mean.evaluate(getAsDoubleArray(rates, true));
        }
        return avaragePurchaseRate.doubleValue();
    }

    @Override
    public double getAvarageSellingRate() {
        if (avarageSelingRate == null) {
            avarageSelingRate = mean.evaluate(getAsDoubleArray(rates, false));
        }
        return avarageSelingRate.doubleValue();
    }

    @Override
    public double getPurchaseRateStandardDeviation() {
        if (purchaseRateStdDev == null) {
            purchaseRateStdDev = new Double(stdDev.evaluate(getAsDoubleArray(rates, true), getAvaragePurchaseRate()));
        }
        return purchaseRateStdDev.doubleValue();
    }

    @Override
    public double getSellingRateStandardDeviation() {
        if (sellingRateStdDev == null) {
            sellingRateStdDev = new Double(stdDev.evaluate(getAsDoubleArray(rates, false), getAvarageSellingRate()));
        }
        return sellingRateStdDev.doubleValue();
    }

    @Override
    public IRate getLowestSellingRate() {
        if (lowestSellingRate == null) {
            setStateToAll(RateComparableStates.COMPARABLE_BY_SELLING_RATE);
            if (!rates.isEmpty()) {
                lowestSellingRate = Collections.min(rates);
            }
        }
        return lowestSellingRate;
    }

    @Override
    public IRate getHighestSellingRate() {
        if (highestSellingRate == null) {
            setStateToAll(RateComparableStates.COMPARABLE_BY_SELLING_RATE);
            if (!rates.isEmpty()) {
                highestSellingRate = Collections.max(rates);
            }
        }
        return highestSellingRate;
    }

    @Override
    public IRate getLowestPurchaseRate() {
        if (lowestPurchaseRate == null) {
            setStateToAll(RateComparableStates.COMPARABLE_BY_PURCHASE_RATE);
            if (!rates.isEmpty()) {
                lowestPurchaseRate = Collections.min(rates);
            }
        }
        return lowestPurchaseRate;
    }

    @Override
    public IRate getHighestPurchaseRate() {
        if (highestPurchaseRate == null) {
            setStateToAll(RateComparableStates.COMPARABLE_BY_PURCHASE_RATE);
            if (!rates.isEmpty()) {
                highestPurchaseRate = Collections.max(rates);
            }
        }
        return highestPurchaseRate;
    }

    @Override
    public IRate getFirstRate() {
        if(firstRate == null) {
            setStateToAll(RateComparableStates.COMPARABLE_BY_DATE);
            firstRate = Collections.min(rates);
        }
        return firstRate;
    }

    @Override
    public IRate getLastRate() {
        if(lastRate == null) {
            setStateToAll(RateComparableStates.COMPARABLE_BY_DATE);
            lastRate = Collections.max(rates);
        }
        return lastRate;
    }

    @Override
    public double getSellingRateChange() {
        if(sellingRateChange == null) {
            sellingRateChange = getFirstRate().getSellingRate() - getLastRate().getSellingRate();
        }
        return sellingRateChange.doubleValue();
    }

    @Override
    public double getPurchaseRateChange() {
        if(purchaseRateChange == null) {
            purchaseRateChange = getFirstRate().getPurchaseRate() - getLastRate().getPurchaseRate();
        }
        return purchaseRateChange;
    }

    private void setStateToAll(RateComparableStates state) {
        for (IRate rate : rates) {
            rate.setComparableState(state);
        }
    }

    private double[] getAsDoubleArray(List<IRate> rates, boolean purchase) {
        double[] asDoubles = new double[rates.size()];
        int i = 0;
        for (IRate rate : rates) {
            double val;
            if (purchase) {
                val = rate.getPurchaseRate();
            } else {
                val = rate.getSellingRate();
            }
            asDoubles[i] = val;
            i++;
        }
        return asDoubles;
    }

    @Override
    public DecimalFormat getSourceDecimalFormat() {
        return srcDecFrm;
    }
}
