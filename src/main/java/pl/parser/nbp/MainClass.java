package pl.parser.nbp;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import pl.parser.nbp.parser.INbpFileParser;
import pl.parser.nbp.handler.NbpSaxHandlerFactory;
import pl.parser.nbp.parser.NbpFileParser;
import pl.parser.nbp.report.ICurrencyRateReport;
import pl.parser.nbp.urifinder.INbpFilesURIsFinder;
import pl.parser.nbp.urifinder.StartDateAfterStopDateException;
import pl.parser.nbp.urifinder.NbpFilesURIsFinder;

public class MainClass {

    private static final String message = "Input arguments list format: CURRENCY_CODE_1 [[CURRENCY_CODE_2] [CURRENCY_CODE_3] ... [CURRENCY_CODE_N]] StartDate StopDate";
    private static final String example = "For example: EUR JPY 2003-11-30 2005-12-03";
    private static final String datePattern = "yyyy-MM-dd";

    public static void main(String[] args) throws MalformedURLException, IOException, ParserConfigurationException {

        // There must be at least 3 arguments
        if (args.length < 3) {
            System.out.println("There should be at least 3 arguments.");
            System.out.println(message);
            System.out.println(example);
            System.exit(-1);
        }
        // Parsing arguments to invoke main.generateReports()
        List<ICurrencyRateReport> reports;
        List<String> curr = new ArrayList<>();
        try {
            for (int i = 0; i < args.length - 2; i++) {
                curr.add(args[i]);
            }
        } catch (Exception ex) {
            System.out.println(message);
            System.out.println(example);
            System.exit(-1);
        }
        MainClass main = new MainClass();
        try {
            reports = main.generateReports(args[args.length - 2], args[args.length - 1], curr);
            main.printResults(reports);
        } catch (Exception ex) {
            System.out.println(ex.getClass());
            System.out.println("Exception message: " + ex.getMessage());
            if (ex instanceof ParseException) {
                System.out.println("Dates format invalid. Valid date format is " + datePattern);
            } else if (ex instanceof StartDateAfterStopDateException) {
                System.out.println("Stop date should be after or equal start date");
            } else if (ex instanceof IOException) {
                System.out.println("Check internet connection");
            } else if (ex instanceof SAXException || ex instanceof ParserConfigurationException) {
                System.out.println("Unexpected problem with parsing NBP xml file has occured");
            }
        }
    }

    /**
     * This method collect currency rates reports for given period of time and given currencies 
     * 
     * @param startDate
     * @param stopDate
     * @param currencyCodes
     * @return List of currency rates reports
     * @throws MalformedURLException
     * @throws IOException
     * @throws ParseException
     * @throws StartDateAfterStopDateException
     * @throws ParserConfigurationException
     * @throws SAXException 
     */
    private List<ICurrencyRateReport> generateReports(String startDate, String stopDate, List<String> currencyCodes) throws MalformedURLException, IOException, ParseException, StartDateAfterStopDateException, ParserConfigurationException, SAXException {

        INbpFilesURIsFinder uriFinder = new NbpFilesURIsFinder();
        List<String> filesURIs = uriFinder.findFilesURIsFilteredByDate(startDate, stopDate, datePattern);
        //listURIs(filesURIs);
        INbpFileParser parser = new NbpFileParser(new NbpSaxHandlerFactory());
        List<ICurrencyRateReport> reportList = parser.parseNbpFiles(filesURIs, currencyCodes);
        return reportList;
    }

    /*
     * This method prints results with proper accuracy based on source values accuracy
     */
    private void printResults(List<ICurrencyRateReport> reports) throws IOException {
        for (ICurrencyRateReport report : reports) {
            String toPrint = "";
            if (reports.size() > 1) {
                toPrint += (report.getCurrencyCode() + "\n");
            }
            toPrint += (report.getSourceDecimalFormat().format(report.getAvaragePurchaseRate()) + "\n" + report.getSourceDecimalFormat().format(report.getSellingRateStandardDeviation()) + "\n");
            System.out.println(toPrint);
        }
        //printTest(reports);
    }

    private void printTest(List<ICurrencyRateReport> reports) {
        String end = "\n";
        for (ICurrencyRateReport report : reports) {
            String out = "";
            out += "Currency code: " + report.getCurrencyCode() + end;
            out += "Avarage purchase rate: " + report.getAvaragePurchaseRate() + end;
            out += "Avarage selling rate: " + report.getAvarageSellingRate() + end;
            out += "Highest purchase rate: " + report.getHighestPurchaseRate() + end;
            out += "Highest selling rate: " + report.getHighestSellingRate() + end;
            out += "Lowest purchase rate: " + report.getLowestPurchaseRate() + end;
            out += "Lowest selling rate: " + report.getLowestSellingRate() + end;
            out += "Purchase rate standard deviation: " + report.getPurchaseRateStandardDeviation() + end;
            out += "Selling rate standard deviation: " + report.getSellingRateStandardDeviation() + end;
            out += "Start date: " + report.getFirstRate().getDate() + end;
            out += "Stop date: " + report.getLastRate().getDate() + end;
            System.out.println(out + end);
        }
    }

    private void listURIs(List<String> filesURIs) {
        System.out.println("URIs:");
        for(String s : filesURIs) {
            System.out.println(s);
        }
    }

}
