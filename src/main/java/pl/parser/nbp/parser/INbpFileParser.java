
package pl.parser.nbp.parser;

import java.io.IOException;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import pl.parser.nbp.report.ICurrencyRateReport;

/**
 *
 * @author marcin
 */
public interface INbpFileParser {

    public List<ICurrencyRateReport> parseNbpFiles(List<String> filensURIs, List<String> currencyCodes) throws IOException, ParserConfigurationException, SAXException;
}
