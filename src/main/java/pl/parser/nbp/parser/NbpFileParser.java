package pl.parser.nbp.parser;

import java.io.IOException;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;
import pl.parser.nbp.handler.AbstractNbpSaxHandler;
import pl.parser.nbp.handler.INbpSaxHandlerFactory;
import pl.parser.nbp.report.ICurrencyRateReport;

/**
 *
 * @author marcin
 */
public class NbpFileParser implements INbpFileParser {

    private INbpSaxHandlerFactory handlerFactory;

    public NbpFileParser(INbpSaxHandlerFactory handlerFactory) {
        this.handlerFactory = handlerFactory;
    }

    /**
     * 
     * @param filesURIs Locations of XML files which contain required exchange rate data
     * @param currencyCodes Codes of currencies for which reports should be generated
     * @return List of currency rate reports
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException 
     */
    @Override
    public List<ICurrencyRateReport> parseNbpFiles(List<String> filesURIs, List<String> currencyCodes) throws SAXException, ParserConfigurationException, IOException {

        AbstractNbpSaxHandler handler = handlerFactory.getInstance(currencyCodes);
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        SAXParser saxParser = saxParserFactory.newSAXParser();

        for (String name : filesURIs) {
            try {
                saxParser.parse(name, handler);
            } catch (SAXException ex) {
                if (!(ex instanceof FoundAllException)) {
                    throw ex;
                }
            }
        }
        return handler.getResults();
    }
}
