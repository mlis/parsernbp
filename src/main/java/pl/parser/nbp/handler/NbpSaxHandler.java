package pl.parser.nbp.handler;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.xml.sax.SAXException;
import pl.parser.nbp.parser.FoundAllException;
import pl.parser.nbp.report.ICurrencyRateReport;
import pl.parser.nbp.report.IRate;
import pl.parser.nbp.report.CurrencyRateReport;
import pl.parser.nbp.report.Rate;

/**
 *
 * @author marcin
 */
public class NbpSaxHandler extends AbstractNbpSaxHandler {

    private final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    private final String nbpDecFrm = ("0.0000");
    private final DecimalFormat decFrm = new DecimalFormat(nbpDecFrm);
    private final String time = "data_publikacji";
    private final String purchase = "kurs_kupna";
    private final String selling = "kurs_sprzedazy";
    private final String multiplier = "przelicznik";
    private final String codeVal = "kod_waluty";
    private Map<String, List<IRate>> codeToRates = new HashMap<>();
    private Map<String, String> codeToDecFormatMap = new HashMap<>();
    private boolean valid = true;
    private int currCodesFound = 0;
    private String currCode;
    private int currMulti;
    private double currPurch;
    private double currSell;
    private String content;
    private Date currDate;
    private List<ICurrencyRateReport> results;

    public NbpSaxHandler(List<String> currencyCodes) {
        for (String s : currencyCodes) {
            codeToRates.put(s, new ArrayList<IRate>());
        }
    }

    @Override
    public void endElement(String uri, String localName, String elementName) throws SAXException {


        switch (elementName) {

            case time:
                try {
                    currDate = formatter.parse(content);
                } catch (ParseException ex) {
                    Logger.getLogger(NbpSaxHandler.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;

            case multiplier:
                currMulti = Integer.valueOf(content);
                break;

            case codeVal:
                currCode = content;
                if (!codeToRates.containsKey(content)) {
                    valid = false;
                }
                break;

            case purchase:
                if (valid) {
                    try {
                        currPurch = parseToDouble(content);
                    } catch (ParseException ex) {
                        Logger.getLogger(NbpSaxHandler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                break;

            case selling:
                if (valid) {
                    try {
                        currSell = parseToDouble(content);
                    } catch (ParseException ex) {
                        Logger.getLogger(NbpSaxHandler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    finish();
                }
                valid = true;
                break;

        }
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        if (valid) {
            content = new String(ch, start, length);
        }
    }

    private void finish() throws FoundAllException {
        codeToRates.get(currCode).add(new Rate(currDate, currPurch, currSell));
        if(!codeToDecFormatMap.containsKey(currCode) && currMulti != 1) {
            // Gnerating patterns with proper accuracy for not default multiliers (!=1)
            String currForm;
            if(currMulti >= 10000) {
                currForm = "0";
            } else {
                StringBuilder builder = new StringBuilder("0.");
                for(int i = 0; i < String.valueOf(currMulti).length() - 1; i++) {
                    builder.append("0");
                }
                currForm = builder.toString();
            }
            codeToDecFormatMap.put(currCode, currForm);
        }
        currCodesFound++;
        if (currCodesFound == codeToRates.size()) {
            currCodesFound = 0;
            throw new FoundAllException();
        }
    }

    private void prepareResults() {
        results = new ArrayList<>();
        for (String key : codeToRates.keySet()) {
            DecimalFormat f;
            if(!codeToDecFormatMap.containsKey(key)) {
                f = decFrm;
            } else {
                f = new DecimalFormat(codeToDecFormatMap.get(key));
            }
            results.add(new CurrencyRateReport(key, codeToRates.get(key), f));
        }
    }
    /**
     * This method converts data collected during parsing and forms reports
     * @return List of currency rate reports
     */
    @Override
    public List<ICurrencyRateReport> getResults() {
        prepareResults();
        return results;
    }

    private double parseToDouble(String cnt) throws ParseException {
        return decFrm.parse(cnt.replaceAll(",", ".")).doubleValue() * currMulti;
    }
}