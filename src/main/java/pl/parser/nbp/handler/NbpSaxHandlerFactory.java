/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.handler;

import java.util.List;
/**
 *
 * @author marcin
 */
public class NbpSaxHandlerFactory implements INbpSaxHandlerFactory {

    @Override
    public AbstractNbpSaxHandler getInstance(List<String> currencyCodes) {
        return new NbpSaxHandler(currencyCodes);
    }
    
}
