/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.parser.nbp.handler;

import java.util.List;
import org.xml.sax.helpers.DefaultHandler;
import pl.parser.nbp.report.ICurrencyRateReport;

/**
 *
 * @author marcin
 */
public abstract class AbstractNbpSaxHandler extends DefaultHandler {
    public abstract List<ICurrencyRateReport> getResults();
}
