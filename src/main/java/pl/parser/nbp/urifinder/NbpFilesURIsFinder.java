package pl.parser.nbp.urifinder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author marcin
 */
public class NbpFilesURIsFinder implements INbpFilesURIsFinder {

    private final String nameSplit = "z";
    private final String nbpUrl = "http://www.nbp.pl/kursy/xml/";
    private final String dir = "dir.txt";
    private final String nbpDateFormat = "yyMMdd";
    private final String suffix = ".xml";
    private final DateFormat nbpDateFormatter = new SimpleDateFormat(nbpDateFormat);
    private final String pattern = "^c.*";
    private final String message = "Start date after stop date!";

    /**
     * This method, based on dir.txt file, finds URIs of files which contain required data (part of each XML file indicates publish data of currency rate)
     * 
     * @param startDate
     * @param stopDate
     * @param sourceDateFormat
     * @return List of URI specifying locations of XML files which contain required data 
     * @throws MalformedURLException
     * @throws IOException
     * @throws ParseException
     * @throws StartDateAfterStopDateException 
     */
    @Override
    public List<String> findFilesURIsFilteredByDate(String startDate, String stopDate, String sourceDateFormat) throws MalformedURLException, IOException, ParseException, StartDateAfterStopDateException {

        DateFormat srcFormatter = new SimpleDateFormat(sourceDateFormat);

        Date startDateAsDate = srcFormatter.parse(startDate);
        Date stopDateAsDate = srcFormatter.parse(stopDate);

        return findFilesURIsFilteredByDate(startDateAsDate, stopDateAsDate);
    }

    /**
     * This method, based on dir.txt file, finds URIs of files which contain required data (part of each XML file indicates publish data of currency rate)
     * 
     * @param startDateAsDate
     * @param stopDateAsDate
     * @return List of URI specifying locations of XML files which contain required data 
     * @throws MalformedURLException
     * @throws IOException
     * @throws ParseException
     * @throws StartDateAfterStopDateException 
     */
    @Override
    public List<String> findFilesURIsFilteredByDate(Date startDateAsDate, Date stopDateAsDate) throws MalformedURLException, IOException, ParseException, StartDateAfterStopDateException {

        if (stopDateAsDate.before(startDateAsDate)) {
            throw new StartDateAfterStopDateException(message);
        }

        List<String> fileURIs = new ArrayList<>();
        URL url = new URL(nbpUrl + dir);
        try (Scanner scanner = new Scanner(url.openStream())) {

            boolean fistFound = false;

            while (scanner.hasNextLine()) {
                String currName = scanner.nextLine();
                if (currName.matches(pattern)) {
                    Date currDate = nbpDateFormatter.parse(currName.split(nameSplit)[1]);
                    if (!fistFound && currDate.compareTo(startDateAsDate) >= 0) {
                        fistFound = true;
                    }
                    if (fistFound) {
                        if (currDate.after(stopDateAsDate)) {
                            break;
                        }
                        fileURIs.add(nbpUrl + currName + suffix);
                    }
                }
            }
        }
        return fileURIs;
    }
}
