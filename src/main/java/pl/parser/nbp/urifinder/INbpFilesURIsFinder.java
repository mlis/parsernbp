
package pl.parser.nbp.urifinder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author marcin
 */
public interface INbpFilesURIsFinder {

    public List<String> findFilesURIsFilteredByDate(String startDate, String stopDate, String sourceDateFormat) throws MalformedURLException, IOException, ParseException, StartDateAfterStopDateException;

    public List<String> findFilesURIsFilteredByDate(Date startDate, Date stopDate) throws MalformedURLException, IOException, ParseException, StartDateAfterStopDateException;

}
