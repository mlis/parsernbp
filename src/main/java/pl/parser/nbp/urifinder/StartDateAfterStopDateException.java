
package pl.parser.nbp.urifinder;

/**
 *
 * @author marcin
 */
public class StartDateAfterStopDateException extends Exception {

    public StartDateAfterStopDateException() {
        super();
    }

    public StartDateAfterStopDateException(String message) {
        super(message);
    }
}
